/* eslint-disable no-empty */
/* eslint-disable max-classes-per-file */
import { Response, Request, NextFunction } from "express";

/**
 * Class representing a bad request error.
 * It extends a generic error.
 */
export class BadParameters extends Error {
  /**
   * Creates a new BadRequest error object.
   *
   * @param {string} message - Bad request message.
   */
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, BadParameters.prototype);
  }
}

/**
 * Class representing a bad request error.
 * It extends a generic error.
 */
export class BadRequest extends Error {
  /**
   * Creates a new BadRequest error object.
   *
   * @param {string} message - Bad request message.
   */
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, BadRequest.prototype);
  }
}

/**
 * Class representing a bad request error.
 * It extends a generic error.
 */
export class Forviden extends Error {
  /**
   * Creates a new Forviden error object.
   *
   * @param {string} message - Forviden message.
   */
  constructor(message: string) {
    super(message);
    Object.setPrototypeOf(this, Forviden.prototype);
  }
}

/**
 * Handles an error during any express api request.
 * It creates a log document in MongoDB.
 * Returns a traceId.
 *
 * @param {Error} err - Exception.
 * @param {Request} req - Express original request.
 * @param {Response} res - Express original response.
 * @param {NextFunction} next - Express next function.
 */
export const handleError = async (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
): Promise<void> => {
  try {
    if (err instanceof BadRequest) {
      res.status(400).json({ error: err.message });
    } else if (err instanceof BadParameters) {
      res.status(404).json({ error: err.message });
    } else if (err instanceof Forviden) {
      res.status(403).json({ error: err.message });
    } else {
      res.status(500).json({ error: err.stack?.toString() || "" });
    }
  } catch (error) {
  } finally {
    next();
  }
};
