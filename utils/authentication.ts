import jwt from "jsonwebtoken";
import { Request, Response } from "express";
import CryptoJS from "crypto-js";
import { IClient } from "../db/client";
import Role from "../data/roles";
import { BadRequest, Forviden } from "./Error";

async function createToken(userObject: object, signOptions?: jwt.SignOptions) {
  return jwt.sign({ userObject }, `${process.env.JWT_SECRET_KEY}`, {
    expiresIn: "1h",
    ...signOptions,
  });
}

async function isLogged(req: Request, res: Response, next: any) {
  try {
    if (req.headers) {
      const token = req.headers["authorization"];
      if (token) {
        jwt.verify(
          token,
          `${process.env.JWT_SECRET_KEY}`,
          (
            err:
              | jwt.JsonWebTokenError
              | jwt.NotBeforeError
              | jwt.TokenExpiredError
              | null
          ) => {
            if (err) {
              next(err);
            } else {
              const tokenInfo = jwt.decode(token);
              req.body.userInfo = tokenInfo;
              next();
            }
          }
        );
      }
    } else {
      throw new BadRequest("headers not provided");
    }
  } catch (error) {
    next(error);
  }
}

function hasPermission(req: Request, roles: Role[]): string {
  if (
    !roles
      .map((role) => role.roleId)
      .includes(req.body.userInfo.userObject.role.roleId)
  ) {
    throw new Forviden("No tiene acceso");
  }
  return req.body.userInfo.userObject.foreignId;
}

/**
 * Encrypts a string.
 */
function encrypt(string: string, cipher?: string) {
  let lCipher: string;
  if (cipher) {
    lCipher = cipher;
  } else {
    lCipher = `${process.env.CRYPYO_SECRET_KEY}`;
  }
  return CryptoJS.AES.encrypt(string, lCipher).toString();
}

/**
 * Decrypts a string.
 */
function decrypt(string: string, cipher?: string) {
  let lCipher: string;
  if (cipher) {
    lCipher = cipher;
  } else {
    lCipher = `${process.env.CRYPYO_SECRET_KEY}`;
  }
  return CryptoJS.AES.decrypt(string, lCipher).toString(CryptoJS.enc.Utf8);
}

export { createToken, isLogged, encrypt, decrypt, hasPermission };
