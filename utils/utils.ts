/**
 * @description Validates a string as an e-mail.
 *
 * @param mail String to validate
 * @returns If the string is a valid e-mail
 * @copyright https://www.w3resource.com/javascript/form/email-validation.php
 */
const validateEmail = (mail: string): boolean => {
  if (
    /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(
      mail
    )
  ) {
    return true;
  }
  return false;
};

export { validateEmail };
