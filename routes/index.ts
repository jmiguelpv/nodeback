import express from "express";

import {
  LessonRouter,
  ClientRouter,
  PsychologistRouter,
  UserRouter,
  ManagerRouter,
} from "../controllers";

const router = express.Router();

router.use("/lessons", LessonRouter);
router.use("/clients", ClientRouter);
router.use("/psychologists", PsychologistRouter);
router.use("/users", UserRouter);
router.use("/managers", ManagerRouter);

export default router;
