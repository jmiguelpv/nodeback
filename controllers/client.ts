import {
  addNewClient,
  buyCourse,
  ownedCourses,
} from "../services/clientService";
import { Request, Response } from "express";
import Role from "../data/roles";
import { hasPermission } from "../utils/authentication";
import { BadParameters } from "../utils/Error";

const registerClient = async (req: Request, res: Response, next: any) => {
  const { mail, name, lastName, password, birthDay, birthMonth, birthYear } =
    req.body;
  try {
    const birth = new Date(birthYear, birthMonth - 1, birthDay);
    const token = await addNewClient(mail, name, lastName, password, birth);
    res.status(201).json({
      token,
      role: Role.User,
    });
    next();
  } catch (error) {
    next(error);
  }
};

const addNewCourse = async (req: Request, res: Response, next: any) => {
  const { courseId } = req.body;
  try {
    const clientId = hasPermission(req, [Role.User]);
    const courseIds = await ownedCourses(clientId);
    if (courseIds?.includes(courseId)) {
      throw new BadParameters("El ciente ya cuenta con este curso");
    }
    buyCourse(clientId, courseId);
    res.status(200).json({
      courseId,
    });
    next();
  } catch (error) {
    next(error);
  }
};

const getOwnedCourses = async (req: Request, res: Response, next: any) => {
  try {
    const clientId = hasPermission(req, [Role.User]);
    const courseIds = await ownedCourses(clientId);
    res.status(200).json({
      courseIds,
    });
    next();
  } catch (error) {
    next(error);
  }
};

export { registerClient, addNewCourse, getOwnedCourses };
