import { createLesson, getRandomLesson } from "../services/lessonService";
import { uploadLessonFile } from "../services/azureBlob";
import FileType from "../data/fileType";
import { Request, Response } from "express";
import { hasPermission } from "../utils/authentication";
import Role from "../data/roles";

const postLesson = async (req: Request, res: Response, next: any) => {
  const { content, lessonName, fileType } = req.body;
  try {
    const psychologistId = hasPermission(req, [Role.Psychologist]);
    const type = FileType.findByName(fileType);
    const resourceUrl = await uploadLessonFile(lessonName, content, type);
    const lessonId = await createLesson(
      lessonName,
      psychologistId,
      type,
      resourceUrl
    );
    res.status(201).json({
      lessonId,
    });
    next();
  } catch (error) {
    next(error);
  }
};

const getLessonRandom = async (req: Request, res: Response, next: any) => {
  try {
    const lessonId = await getRandomLesson();
    res.status(200).json({
      lessonId,
    });
    next();
  } catch (error) {
    next(error);
  }
};

export { postLesson, getLessonRandom };
