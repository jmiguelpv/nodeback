import {
  addCourseLesson,
  createCourse,
  AllCourses,
} from "../services/courseService";
import { Request, Response } from "express";
import { hasPermission } from "../utils/authentication";
import Role from "../data/roles";

const postCreateCouse = async (req: Request, res: Response, next: any) => {
  const { charge, courseName, datesNumber } = req.body;
  try {
    hasPermission(req, [Role.Psychologist]);
    const courseId = await createCourse(courseName, charge, datesNumber);
    res.status(201).json({
      courseId,
    });
    next();
  } catch (error) {
    next(error);
  }
};

const postCourseLesson = async (req: Request, res: Response, next: any) => {
  const { courseId, unlockAfterDates, lessonId } = req.body;
  try {
    hasPermission(req, [Role.Psychologist]);
    await addCourseLesson(courseId, unlockAfterDates, lessonId);
    res.status(201).json({
      courseId,
      lessonId,
    });
    next();
  } catch (error) {
    next(error);
  }
};

const getAllCourses = async (req: Request, res: Response, next: any) => {
  try {
    const courses = await AllCourses();
    res.status(200).json({
      courses,
    });
    next();
  } catch (error) {
    next(error);
  }
};

export { postCreateCouse, getAllCourses, postCourseLesson };
