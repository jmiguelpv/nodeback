import { Request, Response } from "express";
import { logInUser } from "../services/userService";

const logUser = async (req: Request, res: Response, next: any) => {
  try {
    const { mail, password } = req.body;
    const resp = await logInUser(mail, password);
    res.status(201).json(resp);
    next();
  } catch (error) {
    next(error);
  }
};

export { logUser };
