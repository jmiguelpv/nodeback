import {
  addNewPsychologist,
  editSchedule,
} from "../services/psychologistService";
import { Request, Response } from "express";
import Role from "../data/roles";
import { hasPermission } from "../utils/authentication";

const registerPsycologist = async (req: Request, res: Response, next: any) => {
  const { name, lastName, mail, password, birthDay, birthMonth, birthYear } =
    req.body;
  try {
    const managerId = hasPermission(req, [Role.Admin]);
    const birth = new Date(birthYear, birthMonth - 1, birthDay);
    await addNewPsychologist(mail, name, lastName, password, birth, managerId);
    res.status(201).json({
      mail,
      name,
      lastName,
    });
    next();
  } catch (error) {
    next(error);
  }
};

const EditPsycholoistSchedule = async (
  req: Request,
  res: Response,
  next: any
) => {
  const { schedule } = req.body;
  try {
    const psychologistId = hasPermission(req, [Role.Psychologist]);
    const scheduleFormated = await editSchedule(psychologistId, schedule);
    res.status(201).json({
      ...scheduleFormated,
    });
    next();
  } catch (error) {
    next(error);
  }
};

export { registerPsycologist, EditPsycholoistSchedule };
