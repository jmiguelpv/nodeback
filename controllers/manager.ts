import { addNewManager } from "../services/managerService";
import { Request, Response } from "express";
import { hasPermission } from "../utils/authentication";
import Role from "../data/roles";

const registerManager = async (req: Request, res: Response, next: any) => {
  const { name, lastName, mail, password } = req.body;
  try {
    const token = await addNewManager(mail, name, lastName, password);
    res.status(201).json({
      token,
    });
    next();
  } catch (error) {
    next(error);
  }
};

const test = async (req: Request, res: Response, next: any) => {
  try {
    const managerId = hasPermission(req, [Role.Admin]);
    console.log("managerId: ", managerId);
    next();
  } catch (error) {
    next(error);
  }
};

export { registerManager, test };
