import express from "express";
import { isLogged } from "../utils/authentication";

// Lessons endpoints
import { postLesson, getLessonRandom } from "./lessons";

const LessonRouter = express.Router();

LessonRouter.post("/", isLogged, postLesson);
LessonRouter.get("/random", isLogged, getLessonRandom);

// Clientes endpoints
import { registerClient } from "./client";

const ClientRouter = express.Router();

ClientRouter.post("/", registerClient);

// Psychologists endpoints
import { registerPsycologist, EditPsycholoistSchedule } from "./psychologist";

const PsychologistRouter = express.Router();

PsychologistRouter.post("/", isLogged, registerPsycologist);
PsychologistRouter.post("/schedule", isLogged, EditPsycholoistSchedule);

// Users endpoints
import { logUser } from "./user";

const UserRouter = express.Router();

UserRouter.post("/", logUser);

// Managers endpoints
import { registerManager, test } from "./manager";

const ManagerRouter = express.Router();

ManagerRouter.post("/", registerManager);
ManagerRouter.post("/test", isLogged, test);

export {
  LessonRouter,
  ClientRouter,
  PsychologistRouter,
  UserRouter,
  ManagerRouter,
};
