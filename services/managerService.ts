import FileType from "../data/fileType";
import Role from "../data/roles";
import { ManagerModel } from "../db/manager";
import { BadParameters, BadRequest } from "../utils/Error";
import { addNewUser } from "./userService";
import mongoose from "mongoose";

const addNewManager = async (
  mail: string,
  name: string,
  lastName: string,
  password: string
) => {
  if (!mail || !name || !lastName || !password) {
    throw new BadRequest(
      "Para registrar un pasicologo necesita proveer mas datos"
    );
  }
  const document = new ManagerModel();
  document.name = name;
  document.lastName = lastName;

  const session = await mongoose.startSession();
  session.startTransaction();

  const insertedDocument = await ManagerModel.create(document, { session });
  try {
    const token = await addNewUser(
      mail,
      password,
      Role.Admin,
      insertedDocument.id
    );

    await session.commitTransaction();
    session.endSession();

    return token;
  } catch (error) {
    await session.abortTransaction();
    session.endSession();
    throw error;
  }
};

export { addNewManager };
