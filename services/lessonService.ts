import FileType from "../data/fileType";
import { LessonModel } from "../db/lesson";
import { BadRequest } from "../utils/Error";

const createLesson = async (
  lessonName: string,
  psycologistId: string,
  fileType: FileType,
  resourceUrl: string
) => {
  if (!lessonName || !psycologistId || !fileType || !resourceUrl) {
    throw new BadRequest(
      "Para registrar una leccion necesita proveer mas datos"
    );
  }
  const document = new LessonModel();
  document.name = lessonName;
  document.resource = resourceUrl;
  document.creationDateTime = new Date();
  document.creatorId = psycologistId;
  document.fileType = fileType;

  const insertedDocument = await LessonModel.create(document);
  return insertedDocument;
};

const getRandomLesson = async () => {
  const coursesAmount = await LessonModel.find().countDocuments();
  const random = Math.floor(Math.random() * coursesAmount);
  const document = LessonModel.findOne().skip(random);
  return document;
};

export { createLesson, getRandomLesson };
