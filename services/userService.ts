import Role from "../data/roles";
import { UserModel } from "../db/user";
import { createToken, encrypt } from "../utils/authentication";
import { BadParameters, BadRequest } from "../utils/Error";
import EmailValidator from "email-validator";

const addNewUser = async (
  mail: string,
  password: string,
  role: Role,
  foreignId: string
) => {
  if (!mail || !password) {
    throw new BadRequest(
      "Para registrar un usuario necesita proveer correo y contraseña"
    );
  }
  if (!EmailValidator.validate(mail)) {
    throw new BadParameters(`${mail} no es un correo valido.`);
  }
  const alreadyRegistered = await UserModel.findOne({ mail });
  if (alreadyRegistered) {
    throw new BadParameters("Un usuario con ese correo ya fue registado");
  }
  const document = new UserModel();
  document.mail = mail;
  document.password = password;
  document.role = role;
  document.foreignId = foreignId;

  const insertedDocument = await UserModel.create(document);
  const token = await createToken(insertedDocument);
  return token;
};

const logInUser = async (mail: string, password: string) => {
  if (!mail || !password) {
    throw new BadRequest(
      "Para iniciar secion es necesario proporcionar un correo y contraseña"
    );
  }
  const document = await UserModel.findOne({
    mail,
  }).exec();
  if (document) {
    if (document.password === password) {
      const token = await createToken(document);
      return { token, role: document.role };
    }
  }
  throw new BadParameters("Credenciales no validas");
};

export { logInUser, addNewUser };
