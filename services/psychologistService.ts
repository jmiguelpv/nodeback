import FileType from "../data/fileType";
import Role from "../data/roles";
import { ClientModel } from "../db/client";
import { PsychologistModel } from "../db/psychologist";
import { ManagerModel } from "../db/manager";
import { createToken, encrypt } from "../utils/authentication";
import { BadParameters, BadRequest } from "../utils/Error";
import { addNewUser } from "./userService";
import Day from "../data/days";
import Hour from "../data/hours";
import mongoose from "mongoose";

const addNewPsychologist = async (
  mail: string,
  name: string,
  lastName: string,
  password: string,
  birth: Date,
  managerId: string
) => {
  if (!mail || !name || !lastName || !password || !birth) {
    throw new BadRequest(
      "Para registrar un pasicologo necesita proveer mas datos"
    );
  }
  const document = new PsychologistModel();
  document.name = name;
  document.lastName = lastName;
  document.birth = birth;
  const managerDocument = await ManagerModel.findById(managerId);
  if (!managerDocument) {
    throw new Error("El id del administrador no existe");
  }
  const session = await mongoose.startSession();
  session.startTransaction();
  const insertedDocument = await PsychologistModel.create(document, {
    session,
  });
  try {
    const token = await addNewUser(
      mail,
      password,
      Role.Psychologist,
      insertedDocument.id
    );
    await ManagerModel.updateOne(managerDocument, {
      $push: { psycologistIds: insertedDocument.id },
    });

    await session.commitTransaction();
    session.endSession();
    return token;
  } catch (error) {
    await session.abortTransaction();
    session.endSession();

    throw error;
  }
};

const editSchedule = async (psychologistId: string, schedule: any) => {
  if (!psychologistId || !schedule) {
    throw new BadRequest("Para editar un horario");
  }
  const psychologistdocument = await PsychologistModel.findById(psychologistId);
  if (!psychologistdocument) {
    throw new Error("psychologistId not found");
  }

  const scheduleFormated: { day: Day; timetable: Hour[] }[] = [];
  Object.entries(schedule).forEach(([dayString, shcedule]: any) => {
    const timetable: Hour[] = [];
    shcedule.forEach((hour: number) => {
      const hourToInsert = Hour.findById(hour);
      if (hourToInsert) {
        timetable.push(hourToInsert);
      }
    });
    const day = Day.findById(Number.parseInt(dayString));
    if (day) {
      scheduleFormated.push({ day, timetable });
    }
  });

  if (scheduleFormated !== []) {
    await PsychologistModel.updateOne(psychologistdocument, {
      $set: { schedule: scheduleFormated },
    });
  } else {
    await PsychologistModel.updateOne(psychologistdocument, {
      $unset: { schedule: 1 },
    });
  }
  return scheduleFormated;
};

async function impartedCourses(psychologistId: string) {
  const psychologistDocument = await PsychologistModel.findById(psychologistId);
  if (!psychologistDocument) {
    throw new BadParameters(
      `El psicologo id: ${psychologistId} no fue encontrado`
    );
  }
  return psychologistDocument.coursesIds;
}

export { addNewPsychologist, editSchedule, impartedCourses };
