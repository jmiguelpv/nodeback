import FileType from "../data/fileType";
import { CourseModel } from "../db/course";
import { LessonModel } from "../db/lesson";
import { BadParameters, BadRequest } from "../utils/Error";
import { createLesson } from "./lessonService";

const createCourse = async (
  courseName: string,
  charge: number,
  datesNumber: number
) => {
  if (!courseName || !charge || !datesNumber) {
    throw new BadRequest(
      "Para registrar una leccion necesita proveer mas datos"
    );
  }
  const document = new CourseModel();
  document.name = courseName;
  document.charge = charge;
  document.datesNumber = datesNumber;

  const insertedDocument = await CourseModel.create(document);
  return insertedDocument;
};

const addCourseLesson = async (
  courseId: number,
  unlockAfterDates: number,
  lessonId: string
) => {
  if (!courseId || !unlockAfterDates || !lessonId) {
    throw new BadRequest(
      "Para registrar una leccion necesita proveer mas datos"
    );
  }
  const courseDocument = await CourseModel.findById(courseId);
  if (!courseDocument) {
    throw new BadParameters(`El curso id: ${courseId} no fue encontrado`);
  }

  const lesson = LessonModel.findById(lessonId);
  if (!lesson) {
    throw new BadParameters(`La leccion id: ${courseId} no fue encontrada`);
  }

  const lessonToInsert = {
    unlockAfterDates,
    lessonId,
  };
  CourseModel.updateOne(courseDocument, {
    $push: { lessons: lessonToInsert },
  });
};

const AllCourses = async () => {
  const courses = await CourseModel.find();
  return courses.map((course) => course._id);
};

export { createCourse, addCourseLesson, AllCourses };
