import FileType from "../data/fileType";
import { CourseModel } from "../db/course";
import { ClientModel } from "../db/client";
import { PsychologistModel } from "../db/psychologist";
import { DateModel } from "../db/date";
import { LessonModel } from "../db/lesson";
import { BadParameters, BadRequest } from "../utils/Error";
import { createLesson } from "./lessonService";
import Day from "../data/days";
import Hour from "../data/hours";
import { createCourse, addCourseLesson, AllCourses } from "./courseService";
import { datesTaked, ownedCourses } from "./clientService";
import { impartedCourses } from "./psychologistService";

const createDate = async (
  clientId: string,
  psycologistId: string,
  hour: Hour,
  date: Date,
  courseId: string
) => {
  if (!clientId || !psycologistId || !hour || !date || !courseId) {
    throw new BadRequest("Para registrar una cita necesita proveer mas datos");
  }

  const courseDocument = await CourseModel.findById(courseId);
  if (!courseDocument) {
    throw new BadParameters(`El curso id: ${courseId} no fue encontrado`);
  }

  const clientDocument = await ClientModel.findById(clientId);
  if (!clientDocument) {
    throw new BadParameters(`El cliente id: ${clientId} no fue encontrado`);
  }

  const psychologistDocument = await PsychologistModel.findById(psycologistId);
  if (!psychologistDocument) {
    throw new BadParameters(
      `El psicologo id: ${psycologistId} no fue encontrado`
    );
  }

  const day = Day.findById(date.getDay());
  if (!day) {
    throw new Error(`Error en la fecha ${date}`);
  }

  if ((await ownedCourses(clientId))?.includes(courseId)) {
    throw new BadParameters(
      `El cliente id: ${courseId} no ha adquirido el curso  id: ${courseId}`
    );
  }
  if ((await impartedCourses(psycologistId))?.includes(courseId)) {
    throw new BadParameters(
      `El psicologo id: ${psycologistId} no imparte el curso  id: ${courseId}`
    );
  }
  if (
    courseDocument.datesNumber <= (await datesTaked(clientId, courseId)).length
  ) {
    throw new BadParameters(
      `El cliente id: ${courseId} ya tomo todas las citas del curso  id: ${courseId}`
    );
  }

  DateModel.create({ clientId, psycologistId, courseId, day, hour });
};

export { createCourse, addCourseLesson, AllCourses };
