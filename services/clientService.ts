import FileType from "../data/fileType";
import Role from "../data/roles";
import { ClientModel } from "../db/client";
import { CourseModel } from "../db/course";
import { UserModel } from "../db/user";
import { createToken, encrypt } from "../utils/authentication";
import { BadParameters, BadRequest } from "../utils/Error";
import { addNewUser } from "./userService";
import mongoose from "mongoose";
import { DateModel } from "../db/date";

const addNewClient = async (
  mail: string,
  name: string,
  lastName: string,
  password: string,
  birth: Date
) => {
  if (!mail || !name || !lastName || !password || !birth) {
    throw new BadRequest(
      "Para registrar una leccion necesita proveer mas datos"
    );
  }

  const document = new ClientModel();
  document.name = name;
  document.lastName = lastName;
  document.birth = birth;

  const session = await mongoose.startSession();
  session.startTransaction();

  const insertedDocument = await ClientModel.create(document, { session });

  try {
    const token = await addNewUser(
      mail,
      password,
      Role.User,
      insertedDocument.id
    );

    await session.commitTransaction();
    session.endSession();

    return token;
  } catch (error) {
    await session.abortTransaction();
    session.endSession();

    throw error;
  }
};

const ownedCourses = async (clientId: string) => {
  const clientDocument = await ClientModel.findById(clientId);
  if (!clientDocument) {
    throw new BadParameters(`El cliente id: ${clientId} no fue encontrado`);
  }
  return clientDocument.coursesIds;
};

const buyCourse = async (clientId: string, courseId: string) => {
  const clientDocument = ClientModel.findById(clientId);
  if (!clientDocument) {
    throw new BadParameters(`El cliente id: ${clientId} no fue encontrado`);
  }
  const courseDocument = CourseModel.findById(courseId);
  if (!courseDocument) {
    throw new BadParameters(`El cliente id: ${courseId} no fue encontrado`);
  }

  ClientModel.updateOne(clientDocument, {
    $push: { coursesIds: courseId },
  });
};

async function datesTaked(clientId: string, courseId?: string) {
  const quieryDocument = new DateModel();
  quieryDocument.clientId = clientId;
  if (courseId) {
    quieryDocument.courseId = courseId;
  }
  return await DateModel.find(quieryDocument);
}

export { addNewClient, buyCourse, ownedCourses, datesTaked };
