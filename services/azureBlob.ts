import FileType from "../data/fileType";
import fs from "fs";
import {
  BlobServiceClient,
  StorageSharedKeyCredential,
} from "@azure/storage-blob";
import { Readable } from "stream";
import { BadParameters } from "../utils/Error";

const uploadLessonToBlobContainer = async (
  stream: Readable,
  name: string
): Promise<string> => {
  if (name.length <= 3 || name.length >= 100) {
    throw new BadParameters(
      "Name parameter must be string between 3 and 100 characters."
    );
  }

  const account = process.env.BLOB_STORAGE_ACCOUNT_NAME;
  const accountKey = process.env.BLOB_STORAGE_ACCOUNT_KEY;
  if (!account || !accountKey) {
    throw new Error("acount or accountKey undefined.");
  }
  const sharedKeyCredential = new StorageSharedKeyCredential(
    account,
    accountKey
  );
  const blobServiceClient = new BlobServiceClient(
    `https://${account}.blob.core.windows.net`,
    sharedKeyCredential
  );

  const containerClient = blobServiceClient.getContainerClient("lessons");
  const path = "course";
  const blobName = `${path}/${name}`;
  const blockBlobClient = containerClient.getBlockBlobClient(blobName);

  await blockBlobClient.uploadStream(stream, undefined, undefined, {
    blobHTTPHeaders: { blobContentType: "application/pdf" },
  });
  return `https://${account}.blob.core.windows.net/lessons/${blobName}`;
};

const uploadLessonFile = async (
  lessonName: string,
  content: string, //Base 62 file
  fileType: FileType
): Promise<string> => {
  const buffer = Buffer.from(content, "base64");
  const stream = Readable.from(buffer);
  const url = await uploadLessonToBlobContainer(
    stream,
    `${lessonName}${fileType.extension}`
  );
  return url;
};
export { uploadLessonFile };
