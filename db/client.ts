import mongoose, { Schema, Document } from "mongoose";
import { encrypt, decrypt } from "../utils/authentication";
import { validateEmail } from "../utils/utils";

export interface IClient extends Document {
  name: string;
  lastName: string;
  birth: Date;
  coursesIds?: string[];
}

const ClientSchema: Schema = new Schema({
  name: String,
  lastName: String,
  birth: Date,
  coursesIds: { type: [String], default: undefined },
});

export const ClientModel = mongoose.model<IClient>(
  "client",
  ClientSchema,
  "client"
);
