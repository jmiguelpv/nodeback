import mongoose, { Schema, Document } from "mongoose";
import FileType, { FileTypeSchema } from "../data/fileType";

export interface ILesson extends Document {
  name: string;
  resource: string;
  creationDateTime: Date;
  creatorId: string;
  fileType: FileType;
}

const LessonSchema: Schema = new Schema({
  name: String,
  resource: String,
  creationDateTime: Date,
  creatorId: String,
  fileType: FileTypeSchema,
});

export const LessonModel = mongoose.model<ILesson>(
  "lesson",
  LessonSchema,
  "lesson"
);
