import mongoose, { Schema, Document } from "mongoose";

export interface IManager extends Document {
  name: string;
  lastName: string;
  psycologistIds?: string[];
}

const ManagerSchema: Schema = new Schema({
  name: String,
  lastName: String,
  psycologistIds: { type: [String], default: undefined },
});

export const ManagerModel = mongoose.model<IManager>(
  "manager",
  ManagerSchema,
  "manager"
);
