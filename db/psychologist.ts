import mongoose, { Schema, Document } from "mongoose";
import Day, { DaySchema, DayScheduleSchema } from "../data/days";
import Hour, { HourSchema } from "../data/hours";

export interface IPsychologist extends Document {
  name: string;
  lastName: string;
  birth: Date;
  coursesIds: string[];
  schedule: { day: Day; timetable: Hour[] }[];
}

const PsychologistSchema: Schema = new Schema({
  name: String,
  lastName: String,
  birth: Date,
  coursesIds: { type: [String], default: undefined },
  schedule: { type: [DayScheduleSchema], default: undefined },
});

export const PsychologistModel = mongoose.model<IPsychologist>(
  "psychologist",
  PsychologistSchema,
  "psychologist"
);
