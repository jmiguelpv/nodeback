import mongoose, { Schema, Document } from "mongoose";
import Day, { DaySchema, DayScheduleSchema } from "../data/days";
import Hour, { HourSchema } from "../data/hours";

export interface IDate extends Document {
  clientId: string;
  psycologistId: string;
  hour: Hour;
  day: Day;
  courseId: string;
}

const DateSchema: Schema = new Schema({
  clientId: String,
  psycologistId: String,
  day: DaySchema,
  hour: HourSchema,
  courseId: String,
});

export const DateModel = mongoose.model<IDate>("date", DateSchema, "date");
