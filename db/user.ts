import mongoose, { Schema, Document } from "mongoose";
import Role, { RoleSchema } from "../data/roles";
import { encrypt, decrypt } from "../utils/authentication";
import { BadParameters } from "../utils/Error";
import { validateEmail } from "../utils/utils";

export interface IUser extends Document {
  mail: string;
  role: Role;
  password: string;
  foreignId: string;
}

const UserSchema: Schema = new Schema({
  mail: String,
  role: RoleSchema,
  password: String,
  foreignId: String,
});

UserSchema.pre<IUser>(/.*/, function before(this: IUser, next) {
  try {
    // Encriptions
    const cipher = process.env.CIPHER_FRASE ? process.env.CIPHER_FRASE : "";
    if (this.password) {
      const encryptedPassword = encrypt(this.password, cipher);
      this.password = encryptedPassword;
    }

    //Validations
    if (this.mail && !validateEmail(this.mail)) {
      throw new BadParameters("Invalid mail");
    }
    next();
  } catch {}
});

UserSchema.post<IUser>(/.*/, function after(this: IUser, next) {
  const cipher = process.env.CIPHER_FRASE ? process.env.CIPHER_FRASE : "";
  if (this.password) {
    const decryptedPassword = decrypt(this.password, cipher);
    this.password = decryptedPassword;
  }
});

export const UserModel = mongoose.model<IUser>("user", UserSchema, "user");
