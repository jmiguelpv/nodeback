import mongoose, { Schema, Document } from "mongoose";

export interface ICourse extends Document {
  name: string;
  lessons: { lessonId: string; unlockAfterDates: number }[];
  charge: number;
  datesNumber: number;
}

const CourseSchema: Schema = new Schema({
  name: String,
  lessons: {
    type: [{ lessonId: String, unlockAfterDates: Number }],
    default: undefined,
  },
  charge: Number,
  datesNumber: Number,
});

export const CourseModel = mongoose.model<ICourse>(
  "course",
  CourseSchema,
  "course"
);
