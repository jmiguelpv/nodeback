import { Schema } from "mongoose";

/**
 * Enum representing the days of the week.
 */
class Hour {
  // Id of every Hour. Id can be omited and will
  hourId: number;
  // be set automaticly according to indexCounter.
  private static hourIdCounter = 0;

  militaryTime: string;

  worldlyTime: string;

  /**
   * "Enum" definition.
   */
  static readonly Zero = new Hour("00:00", "00:00 AM", 0);
  static readonly One = new Hour("01:00", "01:00 AM", 1);
  static readonly Two = new Hour("02:00", "02:00 AM", 2);
  static readonly Three = new Hour("03:00", "03:00 AM", 3);
  static readonly Four = new Hour("04:00", "04:00 AM", 4);
  static readonly Five = new Hour("05:00", "05:00 AM", 5);
  static readonly Six = new Hour("06:00", "06:00 AM", 6);
  static readonly Seven = new Hour("07:00", "07:00 AM", 7);
  static readonly Eight = new Hour("08:00", "08:00 AM", 8);
  static readonly Nine = new Hour("09:00", "09:00 AM", 9);
  static readonly Ten = new Hour("10:00", "10:00 AM", 10);
  static readonly Eleven = new Hour("11:00", "11:00 AM", 11);
  static readonly Twelve = new Hour("12:00", "12:00 PM", 12);
  static readonly Thirteen = new Hour("13:00", "01:00 PM", 13);
  static readonly Fourteen = new Hour("14:00", "02:00 PM", 14);
  static readonly Fifteen = new Hour("15:00", "03:00 PM", 15);
  static readonly Sixteen = new Hour("16:00", "04:00 PM", 16);
  static readonly Seventeen = new Hour("17:00", "05:00 PM", 17);
  static readonly Eighteen = new Hour("18:00", "06:00 PM", 18);
  static readonly Nineteen = new Hour("19:00", "07:00 PM", 19);
  static readonly Twenty = new Hour("20:00", "08:00 PM", 20);
  static readonly TwentyOne = new Hour("21:00", "09:00 PM", 21);
  static readonly TwentyTwo = new Hour("22:00", "10:00 PM", 22);
  static readonly TwentyThree = new Hour("23:00", "11:00 PM", 23);

  /**
   * Creates a new Hour instance.
   *
   * @param {string} name - Day name.
   * @param {number} id - Day Id.
   */
  private constructor(militaryTime: string, worldlyTime: string, id?: number) {
    if (!id) {
      // eslint-disable-next-line no-plusplus
      this.hourId = Hour.hourIdCounter++;
    } else {
      this.hourId = id;
    }
    this.militaryTime = militaryTime;
    this.worldlyTime = worldlyTime;
  }

  /**
   * 12 hours format.
   *
   * @returns {string} 12 hours format.
   */
  toString12(): string {
    return this.worldlyTime;
  }

  /**
   * 24 hours format.
   *
   * @returns {string} 24 hours format.
   */
  toString24(): string {
    return this.militaryTime;
  }

  /**
   * Gets all static Days instances.
   *
   * @returns {Hour[]} All static Days instances.
   */
  public static getAll(): Hour[] {
    return [
      this.Zero,
      this.One,
      this.Two,
      this.Three,
      this.Four,
      this.Five,
      this.Six,
      this.Seven,
      this.Eight,
      this.Nine,
      this.Ten,
      this.Eleven,
      this.Twelve,
      this.Thirteen,
      this.Fourteen,
      this.Fifteen,
      this.Sixteen,
      this.Seventeen,
      this.Eighteen,
      this.Nineteen,
      this.Twenty,
      this.TwentyOne,
      this.TwentyTwo,
      this.TwentyThree,
    ];
  }

  /**
   * Iterates over all static States instances.
   *
   * @param {Function} callbackfn - Function to be called in foreach loop.
   */
  public static forEach(
    callbackfn: (value: Hour, index: number, array: Hour[]) => void
  ): void {
    this.getAll().forEach(callbackfn);
  }

  /**
   * Finds a Hour by name.
   *
   * @param {string} hourId - Hour Id.
   * @returns {object|null} - Found Hour or null.
   */
  public static findById(hourId: number): Hour | null {
    // eslint-disable-next-line no-restricted-syntax
    for (const status in Hour.getAll()) {
      if (Object.prototype.hasOwnProperty.call(Hour.getAll(), status)) {
        const s = Hour.getAll()[status];
        if (s.hourId === hourId) return s;
      }
    }
    return null;
  }
}

export const HourSchema: Schema = new Schema(
  {
    statusId: Number,
    militaryTime: String,
    worldlyTime: String,
  },
  { _id: false }
);

export default Hour;
