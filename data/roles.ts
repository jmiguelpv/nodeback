import { Schema } from "mongoose";
/**
 * User role types.
 */
class Role {
  // Id of every Role. Id can be omited and will
  roleId: number;
  // be set automaticly according to indexCounter.
  private static dayIdCounter = 0;

  roleName: string;

  /**
   * "Enum" definition.
   */
  static readonly Admin = new Role("Administrador");
  static readonly Psychologist = new Role("Psicologo");
  static readonly User = new Role("Usuario");

  /**
   * Creates a new Role instance.
   *
   * @param {string} name - Day name.
   * @param {number} id - Day Id.
   */
  private constructor(name: string, id?: number) {
    if (!id) {
      this.roleId = Role.dayIdCounter += 1;
    } else {
      this.roleId = id;
    }
    this.roleName = name;
  }

  /**
   * Gets Role instance name.
   *
   * @returns {string} Role name.
   */
  toString(): string {
    return this.roleName;
  }

  /**
   * Gets all static Days instances.
   *
   * @returns {Role[]} All static Days instances.
   */
  public static getAll(): Role[] {
    return [this.Admin, this.Psychologist, this.User];
  }

  /**
   * Iterates over all static States instances.
   *
   * @param {Function} callbackfn - Function to be called in foreach loop.
   */
  public static forEach(
    callbackfn: (value: Role, index: number, array: Role[]) => void
  ): void {
    this.getAll().forEach(callbackfn);
  }

  /**
   * Finds a Role by name.
   *
   * @param {string} name - Role name.
   * @returns {object|null} - Found Role or null.
   */
  public static findByName(name: string): Role | null {
    // eslint-disable-next-line no-restricted-syntax
    for (const status in Role.getAll()) {
      if (Object.prototype.hasOwnProperty.call(Role.getAll(), status)) {
        const s = Role.getAll()[status];
        if (s.roleName === name) return s;
      }
    }
    return null;
  }
}

export const RoleSchema: Schema = new Schema(
  {
    roleId: Number,
    roleName: String,
  },
  { _id: false }
);

export default Role;
