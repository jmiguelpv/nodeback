import { Schema } from "mongoose";
import { HourSchema } from "./hours";
/**
 * Enum representing the days of the week.
 */
class Day {
  // Id of every Day Id can be omited and will
  dayId: number;
  // be set automaticly according to indexCounter.
  private static dayIdCounter = 0;

  name: string;

  /**
   * "Enum" definition.
   */
  static readonly Monday = new Day("Lunes");

  static readonly Tuesday = new Day("Martes");

  static readonly Wednesday = new Day("Miércoles");

  static readonly Thursday = new Day("Jueves");

  static readonly Friday = new Day("Viernes");

  static readonly Saturday = new Day("Sábado");

  static readonly Sunday = new Day("Domingo");

  /**
   * Creates a new Day instance.
   *
   * @param {string} name - Day name.
   * @param {number} id - Day Id.
   */
  private constructor(name: string, id?: number) {
    if (!id) {
      // eslint-disable-next-line no-plusplus
      this.dayId = Day.dayIdCounter++;
    } else {
      this.dayId = id;
    }
    this.name = name;
  }

  /**
   * Gets Day instance name.
   *
   * @returns {string} Day name.
   */
  toString(): string {
    return this.name;
  }

  /**
   * Gets all static Days instances.
   *
   * @returns {Day[]} All static Days instances.
   */
  public static getAll(): Day[] {
    return [
      this.Monday,
      this.Tuesday,
      this.Wednesday,
      this.Thursday,
      this.Friday,
      this.Saturday,
      this.Sunday,
    ];
  }

  /**
   * Iterates over all static Day instances.
   *
   * @param {Function} callbackfn - Function to be called in foreach loop.
   */
  public static forEach(
    callbackfn: (value: Day, index: number, array: Day[]) => void
  ): void {
    this.getAll().forEach(callbackfn);
  }

  /**
   * Finds a Day by name.
   *
   * @param {string} name - Day name.
   * @returns {object|null} - Found Day or null.
   */
  public static findByName(name: string): Day | null {
    // eslint-disable-next-line no-restricted-syntax
    for (const status in Day.getAll()) {
      if (Object.prototype.hasOwnProperty.call(Day.getAll(), status)) {
        const s = Day.getAll()[status];
        if (s.name === name) return s;
      }
    }
    return null;
  }

  /**
   * Finds a Day by id.
   *
   * @param {string} name - Day Id.
   * @returns {object|null} - Found Day or null.
   */
  public static findById(id: number): Day | null {
    // eslint-disable-next-line no-restricted-syntax
    for (const status in Day.getAll()) {
      if (Object.prototype.hasOwnProperty.call(Day.getAll(), status)) {
        const s = Day.getAll()[status];
        if (s.dayId === id) return s;
      }
    }
    return null;
  }
}

export const DaySchema: Schema = new Schema(
  {
    dayId: Number,
    name: String,
  },
  { _id: false }
);

export const DayScheduleSchema: Schema = new Schema(
  {
    day: DaySchema,
    timetable: { type: [HourSchema], default: undefined },
  },
  { _id: false }
);

export default Day;
