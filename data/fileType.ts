import { Schema } from "mongoose";

/**
 * Enum representing the type of a file.
 */
class FileType {
  // Id of every Status. Id can be omited and will
  // be set automaticly according to indexCounter.
  typeId: number;
  private static typeIdCounter = 0;
  name: string;
  extension: string;

  /**
   * "Enum" definition.
   */
  static readonly Unknown = new FileType("?", "", 0);

  static readonly PDF = new FileType("PDF", ".pdf");

  static readonly Img = new FileType("image", ".jpg");

  static readonly Video = new FileType("video", ".mp4");

  /**
   * Creates a new FileType instance.
   *
   * @param {string} name - file type name.
   * @param {string} extension - file type extension.
   * @param {number} id - file type Id.
   */
  private constructor(name: string, extension: string, id?: number) {
    if (!id) {
      this.typeId = FileType.typeIdCounter += 1;
    } else {
      this.typeId = id;
    }
    this.name = name;
    this.extension = extension;
  }

  /**
   * Gets FileType instance extension.
   *
   * @returns {string} FileType extension.
   */
  toString(): string {
    return this.extension;
  }

  /**
   * Gets all static Days instances.
   *
   * @returns {FileType[]} All static Days instances.
   */
  public static getAll(): FileType[] {
    return [this.PDF, this.Img, this.Video, this.Unknown];
  }

  /**
   * Iterates over all static States instances.
   *
   * @param {Function} callbackfn - Function to be called in foreach loop.
   */
  public static forEach(
    callbackfn: (value: FileType, index: number, array: FileType[]) => void
  ): void {
    this.getAll().forEach(callbackfn);
  }

  /**
   * Finds a Status by name.
   *
   * @param {string} name - Status name.
   * @returns {object|null} - Found Status or null.
   */
  public static findByName(name: string): FileType {
    // eslint-disable-next-line no-restricted-syntax
    for (const status in FileType.getAll()) {
      if (Object.prototype.hasOwnProperty.call(FileType.getAll(), status)) {
        const s = FileType.getAll()[status];
        if (s.name === name) return s;
      }
    }
    return FileType.Unknown;
  }
}

export const FileTypeSchema: Schema = new Schema(
  {
    statusId: Number,
    name: String,
    extension: String,
  },
  { _id: false }
);

export default FileType;
