import { config } from "dotenv";
import express from "express";
import bodyParser from "body-parser";
import routes from "./routes";
import { connect } from "./db/database";
import { handleError } from "./utils/Error";

config();
// Connect to DB.
connect();

const app = express();

app.use(bodyParser.json({ limit: "10mb" }));
app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }));

app.get("/", (req, res) => res.send("App is working"));

app.use("/api", routes);

app.use(handleError);

app.listen(3000, () => console.log("Example app listening on port 3000!"));

module.exports = {
  app,
};
